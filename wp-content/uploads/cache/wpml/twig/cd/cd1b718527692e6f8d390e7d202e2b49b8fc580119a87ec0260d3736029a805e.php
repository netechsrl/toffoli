<?php

/* template.twig */
class __TwigTemplate_b89cf194e5e90e7da4d71159a6b8ebc52ae0ac1b031ae1e1f7d8f7afa61434b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["languages"])) { $_languages_ = $context["languages"]; } else { $_languages_ = null; }
        if (isset($context["current_language_code"])) { $_current_language_code_ = $context["current_language_code"]; } else { $_current_language_code_ = null; }
        $context["current_language"] = $this->getAttribute($_languages_, $_current_language_code_, array(), "array");
        // line 2
        if (isset($context["backward_compatibility"])) { $_backward_compatibility_ = $context["backward_compatibility"]; } else { $_backward_compatibility_ = null; }
        $context["css_classes_flag"] = trim(("wpml-ls-flag " . $this->getAttribute($_backward_compatibility_, "css_classes_flag", array())));
        // line 3
        if (isset($context["backward_compatibility"])) { $_backward_compatibility_ = $context["backward_compatibility"]; } else { $_backward_compatibility_ = null; }
        $context["css_classes_native"] = trim(("wpml-ls-native " . $this->getAttribute($_backward_compatibility_, "css_classes_native", array())));
        // line 4
        if (isset($context["backward_compatibility"])) { $_backward_compatibility_ = $context["backward_compatibility"]; } else { $_backward_compatibility_ = null; }
        $context["css_classes_display"] = trim(("wpml-ls-display " . $this->getAttribute($_backward_compatibility_, "css_classes_display", array())));
        // line 5
        if (isset($context["backward_compatibility"])) { $_backward_compatibility_ = $context["backward_compatibility"]; } else { $_backward_compatibility_ = null; }
        $context["css_classes_bracket"] = trim(("wpml-ls-bracket " . $this->getAttribute($_backward_compatibility_, "css_classes_bracket", array())));
        // line 6
        echo "
<div onclick=\"WPMLLanguageSwitcherDropdownClick.toggle(this);\"
\t class=\"";
        // line 8
        if (isset($context["css_classes"])) { $_css_classes_ = $context["css_classes"]; } else { $_css_classes_ = null; }
        echo twig_escape_filter($this->env, $_css_classes_, "html", null, true);
        echo " wpml-ls-legacy-dropdown-click js-wpml-ls-legacy-dropdown-click\"";
        if (isset($context["backward_compatibility"])) { $_backward_compatibility_ = $context["backward_compatibility"]; } else { $_backward_compatibility_ = null; }
        if ($this->getAttribute($_backward_compatibility_, "css_id", array())) {
            echo " id=\"";
            if (isset($context["backward_compatibility"])) { $_backward_compatibility_ = $context["backward_compatibility"]; } else { $_backward_compatibility_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_backward_compatibility_, "css_id", array()), "html", null, true);
            echo "\"";
        }
        echo ">
\t<ul>

\t\t<li class=\"";
        // line 11
        if (isset($context["current_language"])) { $_current_language_ = $context["current_language"]; } else { $_current_language_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_current_language_, "css_classes", array()), "html", null, true);
        echo " wpml-ls-item-legacy-dropdown-click\">

\t\t\t<a href=\"#\" class=\"";
        // line 13
        if (isset($context["current_language"])) { $_current_language_ = $context["current_language"]; } else { $_current_language_ = null; }
        echo twig_escape_filter($this->env, trim(("wpml-ls-item-toggle " . $this->getAttribute($this->getAttribute($_current_language_, "backward_compatibility", array()), "css_classes_a", array()))), "html", null, true);
        echo "\">";
        // line 14
        if (isset($context["current_language"])) { $_current_language_ = $context["current_language"]; } else { $_current_language_ = null; }
        if ($this->getAttribute($_current_language_, "flag_url", array())) {
            // line 15
            echo "<img class=\"";
            if (isset($context["css_classes_flag"])) { $_css_classes_flag_ = $context["css_classes_flag"]; } else { $_css_classes_flag_ = null; }
            echo twig_escape_filter($this->env, $_css_classes_flag_, "html", null, true);
            echo "\" src=\"";
            if (isset($context["current_language"])) { $_current_language_ = $context["current_language"]; } else { $_current_language_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_current_language_, "flag_url", array()), "html", null, true);
            echo "\" alt=\"";
            if (isset($context["current_language_code"])) { $_current_language_code_ = $context["current_language_code"]; } else { $_current_language_code_ = null; }
            echo twig_escape_filter($this->env, $_current_language_code_, "html", null, true);
            echo "\" title=\"";
            if (isset($context["current_language"])) { $_current_language_ = $context["current_language"]; } else { $_current_language_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_current_language_, "flag_title", array()), "html", null, true);
            echo "\">";
        }
        // line 18
        if (isset($context["current_language"])) { $_current_language_ = $context["current_language"]; } else { $_current_language_ = null; }
        if (($this->getAttribute($_current_language_, "display_name", array()) || $this->getAttribute($_current_language_, "native_name", array()))) {
            // line 19
            if (isset($context["current_language"])) { $_current_language_ = $context["current_language"]; } else { $_current_language_ = null; }
            $context["current_language_name"] = (($this->getAttribute($_current_language_, "display_name", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($_current_language_, "display_name", array()), $this->getAttribute($_current_language_, "native_name", array()))) : ($this->getAttribute($_current_language_, "native_name", array())));
            // line 20
            echo "<span class=\"";
            if (isset($context["css_classes_native"])) { $_css_classes_native_ = $context["css_classes_native"]; } else { $_css_classes_native_ = null; }
            echo twig_escape_filter($this->env, $_css_classes_native_, "html", null, true);
            echo "\">";
            if (isset($context["current_language_name"])) { $_current_language_name_ = $context["current_language_name"]; } else { $_current_language_name_ = null; }
            echo twig_escape_filter($this->env, $_current_language_name_, "html", null, true);
            echo "</span>";
        }
        // line 22
        echo "</a>

\t\t\t<ul class=\"wpml-ls-sub-menu\">
\t\t\t\t";
        // line 25
        if (isset($context["languages"])) { $_languages_ = $context["languages"]; } else { $_languages_ = null; }
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($_languages_);
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            if (isset($context["language"])) { $_language_ = $context["language"]; } else { $_language_ = null; }
            if ( !$this->getAttribute($_language_, "is_current", array())) {
                // line 26
                echo "
\t\t\t\t\t<li class=\"";
                // line 27
                if (isset($context["language"])) { $_language_ = $context["language"]; } else { $_language_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_language_, "css_classes", array()), "html", null, true);
                echo "\">
\t\t\t\t\t\t<a href=\"";
                // line 28
                if (isset($context["language"])) { $_language_ = $context["language"]; } else { $_language_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_language_, "url", array()), "html", null, true);
                echo "\">";
                // line 29
                if (isset($context["language"])) { $_language_ = $context["language"]; } else { $_language_ = null; }
                if ($this->getAttribute($_language_, "flag_url", array())) {
                    // line 30
                    echo "<img class=\"";
                    if (isset($context["css_classes_flag"])) { $_css_classes_flag_ = $context["css_classes_flag"]; } else { $_css_classes_flag_ = null; }
                    echo twig_escape_filter($this->env, $_css_classes_flag_, "html", null, true);
                    echo "\" src=\"";
                    if (isset($context["language"])) { $_language_ = $context["language"]; } else { $_language_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_language_, "flag_url", array()), "html", null, true);
                    echo "\" alt=\"";
                    if (isset($context["language"])) { $_language_ = $context["language"]; } else { $_language_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_language_, "code", array()), "html", null, true);
                    echo "\" title=\"";
                    if (isset($context["language"])) { $_language_ = $context["language"]; } else { $_language_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_language_, "flag_title", array()), "html", null, true);
                    echo "\">";
                }
                // line 33
                if (isset($context["language"])) { $_language_ = $context["language"]; } else { $_language_ = null; }
                if ($this->getAttribute($_language_, "native_name", array())) {
                    // line 34
                    echo "<span class=\"";
                    if (isset($context["css_classes_native"])) { $_css_classes_native_ = $context["css_classes_native"]; } else { $_css_classes_native_ = null; }
                    echo twig_escape_filter($this->env, $_css_classes_native_, "html", null, true);
                    echo "\">";
                    if (isset($context["language"])) { $_language_ = $context["language"]; } else { $_language_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_language_, "native_name", array()), "html", null, true);
                    echo "</span>";
                }
                // line 36
                if (isset($context["language"])) { $_language_ = $context["language"]; } else { $_language_ = null; }
                if ($this->getAttribute($_language_, "display_name", array())) {
                    // line 37
                    echo "<span class=\"";
                    if (isset($context["css_classes_display"])) { $_css_classes_display_ = $context["css_classes_display"]; } else { $_css_classes_display_ = null; }
                    echo twig_escape_filter($this->env, $_css_classes_display_, "html", null, true);
                    echo "\">";
                    // line 38
                    if (isset($context["language"])) { $_language_ = $context["language"]; } else { $_language_ = null; }
                    if ($this->getAttribute($_language_, "native_name", array())) {
                        echo "<span class=\"";
                        if (isset($context["css_classes_bracket"])) { $_css_classes_bracket_ = $context["css_classes_bracket"]; } else { $_css_classes_bracket_ = null; }
                        echo twig_escape_filter($this->env, $_css_classes_bracket_, "html", null, true);
                        echo "\"> (</span>";
                    }
                    // line 39
                    if (isset($context["language"])) { $_language_ = $context["language"]; } else { $_language_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_language_, "display_name", array()), "html", null, true);
                    // line 40
                    if (isset($context["language"])) { $_language_ = $context["language"]; } else { $_language_ = null; }
                    if ($this->getAttribute($_language_, "native_name", array())) {
                        echo "<span class=\"";
                        if (isset($context["css_classes_bracket"])) { $_css_classes_bracket_ = $context["css_classes_bracket"]; } else { $_css_classes_bracket_ = null; }
                        echo twig_escape_filter($this->env, $_css_classes_bracket_, "html", null, true);
                        echo "\">)</span>";
                    }
                    // line 41
                    echo "</span>";
                }
                // line 43
                echo "</a>
\t\t\t\t\t</li>

\t\t\t\t";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "\t\t\t</ul>

\t\t</li>

\t</ul>
</div>";
    }

    public function getTemplateName()
    {
        return "template.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 47,  181 => 43,  178 => 41,  170 => 40,  167 => 39,  159 => 38,  154 => 37,  151 => 36,  142 => 34,  139 => 33,  124 => 30,  121 => 29,  117 => 28,  112 => 27,  109 => 26,  102 => 25,  97 => 22,  88 => 20,  85 => 19,  82 => 18,  67 => 15,  64 => 14,  60 => 13,  54 => 11,  39 => 8,  35 => 6,  32 => 5,  29 => 4,  26 => 3,  23 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "template.twig", "/Applications/XAMPP/xamppfiles/htdocs/netech-toffoli/wp-content/plugins/sitepress-multilingual-cms/templates/language-switchers/legacy-dropdown-click/template.twig");
    }
}
