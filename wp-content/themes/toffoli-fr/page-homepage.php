<?php 
/*
	Template Name: Homepage
*/


get_header(); 

?>


    <section class="homepage scroll">
        <div class="container">
            <div class="tabella">
                <div class="cella-tabella">
                    <small><?php the_field('testo_piccolo'); ?></small>
                    <h1><?php the_field('testo_grande'); ?></h1>
                </div>
            </div>
        </div>
        <div class="arrow">
            <i class="fa fa-angle-down" aria-hidden="true"></i><br>
            <i class="fa fa-angle-down" aria-hidden="true"></i><br>
            <i class="fa fa-angle-down" aria-hidden="true"></i>
        </div>
    </section>
    <section class="our-sections normal">
        <div class="container">
            <div class="tabella">
                <div class="cella-tabella">
                    <small><?php the_field('testo_piccolo_secondario'); ?></small>
                    <h1><?php the_field('testo_grande_secondario'); ?></h1>
                    <div class="sezioni clearfix" align="center">
                    <?php 
					$args = array(
					'post_type' => 'settore',
					'orderby' => 'id', 
					'order' => 'ASC', 
					'post_status' => 'publish',
					'posts_per_page' => -1
					);
					$settori = new WP_Query( $args );	
					$i=0;
					while ($settori->have_posts()): $settori->the_post();
					$i++;
					$back_img = get_the_post_thumbnail_url();
					if (get_field('visibile')=="no"){ } else{
					?>
                    
                    
                        <div id="settore<?php echo $i ?>" class="thumb settore" data-ref="<?php echo $back_img ?>">
                            <div class="immag">
                                <img src="<?php echo get_field('icona'); ?>">
                            </div>
                            <h6><?php the_title(); ?></h6>
                            <div class="hover-hidden">
                                <a href="<?php the_permalink() ?>"><div class="button"><?php _e('Go to','netech') ?></div></a>
                                <p>
								<?php
								// check if the repeater field has rows of data
								if( have_rows('prodotti_settori') ):
									// loop through the rows of data
									while ( have_rows('prodotti_settori') ) : the_row();
								?>
									<?php the_sub_field('prodotto_settore');?><br />
								<?php
									endwhile;
								endif;
								?>
								</p>
                            </div>
                        </div>
                        
					<?php } endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
    <script>
	$.each($('.settore'),function(idx,value){

//var img = $(value).attr('data-ref');
 		$(value).hover(function() { 
		    jQuery('.our-sections').css("background-image", "url("+$(value).attr('data-ref')+")"); 
		});
	});

    </script>

