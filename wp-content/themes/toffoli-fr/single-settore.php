<?php 
/*
	Single page: Settore post type
*/


get_header(); 


while ( have_posts() ) : the_post();
?>


    <section class="testata scroll">
        <div class="container">
            <div class="tabella">
                <div class="cella-tabella">
                    <div class="immag-testa">
                        <img src="<?php echo get_field('icona'); ?>">
                    </div>
                     <small><?php echo get_field('testo_piccolo'); ?></small>
                    <h1><?php echo get_field('testo_grande'); ?></h1>
                </div>
            </div>
        </div>
    </section>
    <section class="normal">
        <div class="container">
            <div class="tabella">
                <div class="cella-tabella">
                    <div class="row">
                        <div class="col m8 offset-m2 s10 offset-s1 paragrafo">
                            <img src="<?php echo str_replace('.png', '-b.png', get_field('icona')); ?>">
                            <h3><?php _e('Sector','netech') ?> <?php the_title() ?><br><?php _e('Introduction','netech') ?></h3>
                            <?php  the_content(); ?>
                            <?php if( have_rows('cataloghi') ):?>
                            <ul class="certificati">
                            <?php $i=0;
							while ( have_rows('cataloghi') ) : the_row();
							$i++;?>
                                <li><a href="<?php the_sub_field('catalogo') ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/pdf.png"> <span><?php _e('Download our PDF catalogue','netech') ?> <?php if ($i>1){ echo '('.$i.')'; } ?></span></a></li>
                            </ul>
                            <?php 
							endwhile;
							endif;
							?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php $images = get_field('gallery'); 
		if( $images ){
		?>
        <div class="blocchi clearfix">
            <div class="container">
                <div class="row">
                <?php
				$i=0;
				if( $images ): 
				foreach( $images as $image ): 
				$i++;
				if($i & 1){
				$first = 'first';
				}else{
				$first = '';
				}
				if($i==1){
				?>
                    <div class="box <?php echo $first ?>">
                        <div class="content">
                            <a class="nivo" data-lightbox-gallery="gallery1" href="<?php echo $image['url']; ?>">
                                <div class="title-box parallax-window" data-parallax="scroll" data-image-src="<?php echo $image['url']; ?>">
                                    <div class="blu">
                                        <p><?php the_field('titolo_gallery_1'); ?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
				<?php
				} else {
				?>
				<a class="nivo" data-lightbox-gallery="gallery1" href="<?php echo $image['url']; ?>"></a>
                <?php 
				}
				endforeach;
				endif;
				?>
                <?php $images2 = get_field('gallery_2'); 
				$i=0;
				if( $images2 ): 
				foreach( $images2 as $image3 ): 
				$i++;
				if($i & 1){
				$first = 'first';
				}else{
				$first = '';
				}
				if($i==1){
				?>
                    <div class="box">
                        <div class="content">
                            <a class="nivo" data-lightbox-gallery="gallery2" href="<?php echo $image3['url']; ?>">
                                <div class="title-box parallax-window" data-parallax="scroll" data-image-src="<?php echo $image3['url']; ?>">
                                    <div class="blu">
                                        <p><?php the_field('titolo_gallery_2'); ?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
				<?php
				} else {
				?>
				<a class="nivo" data-lightbox-gallery="gallery2" href="<?php echo $image3['url']; ?>"></a>
                <?php 
				}
				endforeach;
				endif;
				?>
                <?php $images3 = get_field('gallery_3'); 
				$i=0;
				if( $images3 ): 
				foreach( $images3 as $image3 ): 
				$i++;
				if($i & 1){
				$first = 'first';
				}else{
				$first = '';
				}
				if($i==1){
				?>
                    <div class="box <?php echo $first ?>">
                        <div class="content">
                            <a class="nivo" data-lightbox-gallery="gallery3" href="<?php echo $image3['url']; ?>">
                                <div class="title-box parallax-window" data-parallax="scroll" data-image-src="<?php echo $image3['url']; ?>">
                                    <div class="blu">
                                        <p><?php the_field('titolo_gallery_3'); ?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
				<?php
				} else {
				?>
				<a class="nivo" data-lightbox-gallery="gallery3" href="<?php echo $image3['url']; ?>"></a>
                <?php 
				}
				endforeach;
				endif;
				?>
                <?php $images4 = get_field('gallery_4'); 
				$i=0;
				if( $images4 ): 
				foreach( $images4 as $image4 ): 
				$i++;
				if($i==1){
				?>
                    <div class="box">
                        <div class="content">
                            <a class="nivo" data-lightbox-gallery="gallery4" href="<?php echo $image4['url']; ?>">
                                <div class="title-box parallax-window" data-parallax="scroll" data-image-src="<?php echo $image4['url']; ?>">
                                    <div class="blu">
                                        <p><?php the_field('titolo_gallery_4'); ?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
				<?php
				} else {
				?>
				<a class="nivo" data-lightbox-gallery="gallery4" href="<?php echo $image4['url']; ?>"></a>
                <?php 
				}
				endforeach;
				endif;
				?>
                </div>
            </div>
        </div>
		<?php }?>
    </section>
<?php endwhile; ?>
    <div class="form">
        <div class="container">
            <div class="row">
                
                <?php include 'contact-form.php';?>
                
            </div>
        </div>
    </div>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
   