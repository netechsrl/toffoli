<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title(''); // stay compatible with SEO plugins ?></title>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/bower_components/materialize/dist/css/materialize.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/bower_components/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/bower_components/nivo-lightbox/dist/nivo-lightbox.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/bower_components/nivo-lightbox/themes/default/default.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/css/main.css">
      <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRcaYxsK8NP7hot_vYgwDJVBujJyKJl7I&callback=initMap" type="text/javascript"></script>
</head>

<body <?php if (is_singular( 'settore' )) : ?>style="background-image: url(<?php echo the_post_thumbnail_url() ?>); background-size: contain; background-color: black; background-repeat: no-repeat;" <?php endif ?>>
    <header style=" position: absolute; top: 25px; width: 100%;">
        <div class="grey-bar clearfix">
            <div class="container">

            <!-- Dropdown Structure -->
<!--            <nav>
              <div class="nav-wrapper">
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="fa fa-bars" aria-hidden="true"></i></a>
              
              
              </div>
            </nav>
-->            <nav>
				<div class="nav-wrapper" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
                
                <a href="<?php echo home_url() ?>" class="brand-logo"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo.png"></a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="fa fa-bars" aria-hidden="true"></i></a>
                
     
        <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'bootstrapmenu',
                        'depth' => 2,
                        'container' => false,
                        'menu_class' => 'right hide-on-med-and-down',
                        'fallback_cb' => 'wp_page_menu',
                        //Process nav menu using our custom nav walker
                        'walker' => new wp_bootstrap_navwalker())
                    );
                    ?>
					<a href="http://goo.gl/aL6hVI" target="_blank"><img src="http://www.toffoli.it/new/wp-content/uploads/2017/04/business-view.png" alt="Toffoli Google Business View" class="business_view" /></a>
                </div><!-- /.navbar-collapse -->
                </div>
              <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'depth' => 2, 'menu_class' => 'side-nav', 'menu_id' => 'mobile-demo', 'walker' => new wp_bootstrap_navwalker() ) ); ?>

            </nav>
        </div>
      </div>  
        
    </header>