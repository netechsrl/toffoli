<?php 
/*
	Template Name: Catalogo
*/


get_header(); 

?>

<?php 
if ( have_posts() ) : 
while ( have_posts() ) : the_post(); 

?>
    <section class="azienda scroll" style="background-image:url(<?php echo get_field('background_video') ?>)">
        <div class="container">
            <div class="tabella">
                <div class="cella-tabella" style="padding: 100px 0 25px 0;">
                    <?php echo the_content(); ?>
                </div>
            </div>
        </div>
    </section>
<?php 
endwhile;
endif ?>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
   