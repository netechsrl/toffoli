<?php
/**
 * Toffoli-fr WP functions and definitions
 *
 *
 */
?>
<?php

add_filter( 'upload_mimes', 'generate_svg' );
function generate_svg( $svg_mime ) {
    $svg_mime['svg'] = 'image/svg+xml';
    return $svg_mime;
}

/*
Menu
*/
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'extra-menu' => __( 'Extra Menu' ),
	  'bootstrapmenu'=> __( 'Menu Bootstrap' )
	  
    )
  );
}
add_action( 'init', 'register_my_menus' );

// Registriamo un Custom Navigation Walker( Per gestire un menu personalizzato con gli stili di Toffoli)
require_once('wp_bootstrap_navwalker.php');

function wp_nav_menu_attributes_filter($var) {
	return is_array($var) ? array_intersect($var, array('current-menu-item')) : '';
}
add_filter('nav_menu_css_class', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('page_css_class', 'wp_nav_menu_attributes_filter', 100, 1);


/*
Featured image
*/
add_theme_support( 'post-thumbnails' );


/*
Ordiniamo la tassonomia per gerarchia
*/
function hierarchy_order_taxonomy() {
    $out = '';

    global $post;

    $taxonomies = get_object_taxonomies( $post );

    foreach ( $taxonomies as $taxonomy ) {  
        $taxonomy = get_taxonomy( $taxonomy );  
        if ( $taxonomy->query_var && $taxonomy->hierarchical ) {

            $out .= '<ol class="breadcrumb link-black">';
                //$out .=  . $taxonomy->labels->name . '</li>';

                $terms = wp_get_post_terms( $post->ID, $taxonomy->name, array( 'orderby' => 'term_id' ) );
                foreach ( $terms as $term ) {
					$term_link = get_term_link( $term );
                    $out .= '<li><a href="' . esc_url( $term_link ) . '">' . $term->name. '</a></li>';

                }
            $out .= '</ol>';
        }
    }

    return $out;
}



?>
