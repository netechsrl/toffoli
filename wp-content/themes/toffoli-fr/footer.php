    <footer class="normal clearfix">
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col l4 m4 s12">
                        <img class="responsive-img" src="<?php echo get_stylesheet_directory_uri() ?>/img/logo.png">
                        <p>La Francesco Toffoli Spa è specializzata nella lavorazione e trasformazione del filo di acciaio.</p>
                    </div>
                    <div class="col l4 m4 s12">
                        <h5>Contatti</h5>
                        <p>Via Garibaldi, 37 
                        31016 Cordignano (TV)<br>T. +39 0438.99.54.50<br>
                        F. +39 0438.99.54.30 
                        <a href="mailto:info@toffoli.it">
    info@toffoli.it</a></p>
                    </div>
                    <div class="col l2 m4 s12">
                        <h5>Social</h5>
                        <ul class="social">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i> Pagina Facebook</a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> Pagina Twitter</a></li>
                        </ul>
                    </div>
                    <div class="col l12 m12 s12">
                        <h5>Newsletter</h5>
                        <p>Iscriviti alla nostra newsletter lasciando il tuo indirizzo mail</p>
                        <div id="newsletter" class="clearfix">
                            <input placeholder="la tua email" type="text"><button><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                        </div>
                        <p>Scopri le offerte e le promozioni che offre Toffoli</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="pie">
            <div class="container">
                <p>@ Toffoli S.p.A. Tutti i diritti riservati  |  P.iva IT01151930268  |  <a href="http://www.toffoli.it/new/privacy/">Privacy & Policy</a>  |   Web site by <a id="redNetech" href="http://www.netech.it/" target="_blank">Netech</a></p>
            </div>
        </div>
    </footer>
</body>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/bower_components/materialize/dist/js/materialize.min.js"></script>
    <?php if (!is_page_template( 'page-contatti.php' )){?>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/bower_components/Scrollify/jquery.scrollify.min.js"></script>
    <?php } ?>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/bower_components/nivo-lightbox/dist/nivo-lightbox.min.js"></script>
    <?php if (!is_page_template( 'page-contatti.php' )){?>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/js/scroll-custom.js"></script>
     <?php } ?>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/bower_components/parallax.js/parallax.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/js/main.js"></script>
    <script>$('.parallax-window').parallax({speed: 0.8,});</script>
    <script> 
    $(document).ready(function(){ 
        $('.nivo').nivoLightbox(); 
    }); 
    </script>
    <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(45.942709, 12.418896), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(45.942709, 12.418896),
                    map: map,
                    title: 'Toffoli s.p.a.'
                });
            }
        </script>
    
    
</html>
