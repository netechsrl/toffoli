<?php 
/*
	Template Name: Azienda
*/


get_header(); 

?>

<?php 
if ( have_posts() ) : 
while ( have_posts() ) : the_post(); 

?>
    <section class="azienda scroll" style="background-image:url(<?php echo get_field('background_video') ?>)">
        <div class="container">
            <div class="tabella">
                <div class="cella-tabella">
                    <small><?php echo get_field('testo_piccolo'); ?></small>
                    <h1><?php echo get_field('testo_grande'); ?></h1>
                    <?php if  (get_field('video') ){?>
                    <a id="play" class="nivo" href="<?php echo get_field('video'); ?>"><i class="fa fa-play-circle" aria-hidden="true"></i></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <section class="story normal" style="background-image:url(<?php echo the_post_thumbnail_url() ?>)">
        <div class="container">
            <div class="tabella">
                <div class="cella-tabella">
                    <div class="row">
                        <div class="col m8 offset-m2 s10 offset-s1 paragrafo">
                            <?php echo the_content(); ?>
                            
                            
                            <?php 
							if( have_rows('certificati__documenti') ):?>
                            <ul class="certificati">
                            <?php 
							while ( have_rows('certificati__documenti') ) : the_row();
							$i++;?>
                                <li><a href="<?php echo the_sub_field('certificatodocumento') ?>" target="_blank"><img src="<?php echo the_sub_field('icona') ?>"> <span><?php echo the_sub_field('titolo') ?></span></a></li>
                            
                            <?php 
							endwhile;
							endif;
							?>
                            </ul>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php 
endwhile;
endif ?>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
   