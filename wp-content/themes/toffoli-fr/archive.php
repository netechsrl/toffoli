<?php
/**
 * The template for displaying archive pages
 *
 
 */

get_header(); 

$loop = new WP_Query(array(
            'post_type' => 'post'
        ));
?>

    <section class="normal">
        <div class="container">
       
                <div class="contenitore" id="news">
                    <div class="row">
                        <div class="col m12 s12 paragrafo center-align">
                            <div class="immag-testa">
                                <img src="<?php echo get_field('icona'); ?>">
                            </div>
                            <small><?php echo get_field('sottotitolo'); ?></small>
                            <h1><?php wp_title('') ?></h1>
                        </div>
                    </div>
                </div>
        </div>
        <div class="blocchi clearfix">
         <?php if ( $loop->have_posts() ) : ?>
         <div class="container">
                <div class="row">
                <?php
			// Start the Loop.
			$i=0;
			while ( $loop->have_posts() ) : $loop->the_post(); 
			$i++;
			if($i & 1){
				$first = 'first';
			}else{
				$first = '';
			}
			?>
                    <div class="box news <?php echo $first ?>">
                        <div class="content">
                            <a href="<?php the_permalink() ?>">
                                <div class="title-box parallax-window" data-parallax="scroll" data-image-src="<?php echo the_post_thumbnail_url() ?>">
                                    <div class="blu">
                                        <p><span class="pubb"><?php the_time ('d-m-Y') ?></span>
                                            <?php  the_title() ?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    
             <?php endwhile; 
			 // Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
				'next_text'          => __( 'Next page', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
			) );

			 
			 ?>
                </div>
            </div>
        <?php endif; ?>    
        </div>
    </section>


	
<?php get_footer(); ?>
