<?php
/**
 * The template for displaying archive pages
 *
 
 */

get_header(); 


?>

    <section class="normal">
        <div class="container">
       <?php if ( have_posts() ) : 
	   			while ( have_posts() ) : the_post();?>
                <div class="contenitore" id="news">
                    <div class="row">
                        <div class="col m8 offset-m2 s10 offset-s1 paragrafo">
                            <div class="back"><a href="<?php echo home_url( '/news' ); ?>">« TORNA INDIETRO</a></div>
                            <div class="data-news"><?php the_time ('d-m-Y') ?></div>
                            <h3><?php  the_title() ?></h3>
                            <?php the_content(); ?>
                            <?php if (get_field('condivisione_social')){ ?>
                            <ul class="condividi">
                                <li>Condividi:</li>
                                <!--<li><a class="nivo" data-lightbox-type="iframe" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="Share on Facebook." ><i class="fa fa-facebook" aria-hidden="true"></i></a></li>-->
                                <li><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="Share on Facebook." onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>" title="Tweet this!" nclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;title=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" title="Share on LinkedIn" nclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            </ul>
                            <?php }?>
                        </div>
                    </div>
                </div>
        </div>
     	<div class="blocchi clearfix">
            <div class="container">
                <div class="row">
                <?php $images = get_field('gallery'); 
				$i=0;
				if( $images ): 
				foreach( $images as $image ): 
				$i++;
				if($i & 1){
					$first = 'first';
				}else{
					$first = '';
				}
				?>
                    <div class="box <?php echo $first ?>">
                        <div class="content">
                            <a class="nivo" data-lightbox-gallery="gallery1" href="<?php echo $image['url']; ?>">
                                <div class="title-box parallax-window" data-parallax="scroll" data-image-src="<?php echo $image['url']; ?>">
                                    <div class="blu">
                                        <p><?php echo $image['caption']; ?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php 
				endforeach;
				endif;
				?>    
                </div>
            </div>
        </div>
        <?php 
		 endwhile; 
		endif; ?>    
        
    </section>


	
<?php get_footer(); ?>
