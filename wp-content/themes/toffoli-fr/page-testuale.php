<?php 
/*
	Template Name: Testuale
*/


get_header(); 

?>

<?php 
if ( have_posts() ) : 
while ( have_posts() ) : the_post(); 

?>
    <section class="azienda scroll">
        <div class="container">
            <div class="tabella">
                <div class="cella-tabella" style="padding: 150px 0 25px 0;">
                     <?php echo the_content(); ?>
                </div>
            </div>
        </div>
    </section>
<?php 
endwhile;
endif ?>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
   