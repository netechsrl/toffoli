<?php 
/*
	Template Name: Contatti
*/


get_header(); 

?>

<?php 
if ( have_posts() ) : 
while ( have_posts() ) : the_post(); 

?>

    <section class="testata scroll">
        <div id="map"></div>
    </section>
    <section class="normal clearfix">
        <div class="container">
            <div class="contenitore">
                    <div class="row">
                        <div class="col m4 s12 paragrafo center-align">
                            <p><i class="fa fa-map-marker" aria-hidden="true"></i> Via Garibaldi, 37 - 31016 Cordignano (TV) - ITALY</p>
                        </div>
                        <div class="col m4 s12 paragrafo center-align">
                            <p><i class="fa fa-phone" aria-hidden="true"></i> +39 0438.99.54.50 </p>
                        </div>
                        <div class="col m4 s12 paragrafo center-align">
                            <p><i class="fa fa-fax" aria-hidden="true"></i> +39 0438.99.54.30</p>
                        </div>
                        <div class="col m12 s12 paragrafo center-align">
                            <p><a href="mailto:info@toffoli.it"><i class="fa fa-envelope" aria-hidden="true"></i> info@toffoli.it</a></p>
                        </div>
                    </div>
            </div>
        </div>
        <div class="arrow no-absolute">
            <i class="fa fa-angle-down" aria-hidden="true"></i><br>
            <i class="fa fa-angle-down" aria-hidden="true"></i><br>
            <i class="fa fa-angle-down" aria-hidden="true"></i>
        </div>
    </section>
    <div class="form">
        <div class="container">
            <div class="row">
                
                <?php include 'contact-form.php';?>
                
            </div>
        </div>
    </div>

<?php 
endwhile;
endif ?>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
   